<!DOCTYPE html>
<html>
<head>
    <title>Настройка базы данных Блога</title>
</head>
<body>

<h3>Настройка...</h3>

<?php
require_once 'db_config.php';
require_once 'functions.php';
$connection = new mysqli($db_host, $db_user, $db_pass);
if ($connection->connect_error) {
    die ('Ошибка ' . $connection->connect_errno . ' при подключении базы данных.<br>Описание: '. $connection->connect_error);
}

createDATABASE($connection, $db_name);
$connection->close();
?>

<br>...Этап создания БД завершен!<br><br>

<?php
$connection = new mysqli($db_host, $db_user, $db_pass, $db_name);
if ($connection->connect_error) {
    die ('Ошибка ' . $connection->connect_errno . ' при подключении базы данных.<br>Описание: '. $connection->connect_error);
}
createTable($connection, 'users',
                  'id INT UNSIGNED AUTO_INCREMENT KEY,
                        login VARCHAR(16),
                        password VARCHAR(32) NOT NULL');

createTable($connection, 'posts',
                     'id INT UNSIGNED AUTO_INCREMENT KEY,
                            user_id INT,
                            title VARCHAR(64),
                            text TEXT,
                            date DATETIME');

createTable($connection, 'comments',
    'id INT UNSIGNED AUTO_INCREMENT KEY,
                            post_id INT,
                            user_id INT,
                            text TEXT,
                            date DATETIME');
$connection->close();
?>

<br>...Этап создания таблицы завершен!

</body>
</html>