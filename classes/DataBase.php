<?php

class DataBase
{
    private $_connection; // Идентификатор соединения

    public function __construct($host, $user, $password, $dbName)
    {
        $this->_connection = new mysqli($host, $user, $password, $dbName);
        $this->_connection->query("SET NAMES 'utf-8'");
    }

    public function lastInsertId()
    {
        return $this->_connection->insert_id;
    }

    public function query($query)
    {
        $result = $this->_connection->query($query);
        if ($this->_connection->errno) {
            echo $this->_connection->error;
        }
        return $result;
    }

    public function queryAll($query)
    {
        $result = $this->query($query);
        if (!$result) {
            return false;
        }
        $dataAssocArray = [];
        while ($row = $result->fetch_assoc()) {
            array_push($dataAssocArray, $row);
        }
        return $dataAssocArray;
    }

    public function queryRow($query)
    {
        $result = $this->query($query);
        if (!$result) {
            return false;
        }
        return $result->fetch_assoc();
    }

    public function queryScalar($query)
    {
        $row = $this->queryRow($query);
        if (!$row){
            return false;
        }
        return reset($row);
    }
}