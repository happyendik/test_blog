<?php

class Post extends Entry
{
    protected $_fields = [
        'id',
        'user_id',
        'title',
        'text',
        'date',
    ];

    public static $tableName = "posts";

    public function getComments()
    {
        return Comment::findAllByAttribute($this->_db, 'post_id', $this->id);
    }
}
