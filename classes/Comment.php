<?php

require_once 'Entry.php';

class Comment extends Entry
{
    protected $_fields = [
        'id',
        'post_id',
        'user_id',
        'text',
        'date',
    ];

    public static $tableName = "comments";

    public function getUser()
    {
        return User::findOneByAttribute($this->_db, 'id', $this->user_id);
    }
}
