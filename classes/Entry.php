<?php

class Entry
{
    /**
     * @var DataBase
     */
    protected $_db;

    protected $_fields = [
    ];

    protected $_values = [
    ];

    public function __construct($db)
    {
            $this->_db = $db;
    }

    public function __get($name)
    {
        if (!in_array($name, $this->_fields)) {
            throw new Exception;
        }
        if (isset($this->_values[$name])) {
            return $this->_values[$name];
        }
    }

    public function __set($name, $value)
    {
        if (!in_array($name, $this->_fields)) {
            throw new Exception;
        }
        $this->_values[$name] = $value;
    }

    public static function findOneByAttribute($db, $name, $value)
    {
        $row = $db->queryRow("SELECT * FROM ".static::$tableName." WHERE $name='$value'");
        if (!$row) {
            return null;
        }

        return static::createInstance($db, $row);

    }

    public static function findAllByAttribute(DataBase $db, $name, $value)
    {
        $rows = $db->queryAll("SELECT * FROM ".static::$tableName." WHERE $name='$value'");
        if (!$rows) {
            return [];
        }
        $arrayOfObjects = [];
        foreach ($rows as $row) {
            array_push($arrayOfObjects, static::createInstance($db, $row));
        }
        return $arrayOfObjects;
    }

    public static function findAll(DataBase $db)
    {
        $rows = $db->queryAll("SELECT * FROM ".static::$tableName);
        if (!$rows) {
            return [];
        }
        $arrayOfObjects = [];
        foreach ($rows as $row) {
            array_push($arrayOfObjects, static::createInstance($db, $row));
        }
        return $arrayOfObjects;

    }

    public static function findOne($db, $id)
    {
        return static::findOneByAttribute($db, 'id', $id);
    }

    public function load($array = [])
    {
        if ($array) {
            foreach ($array as $key => $value){
               if(in_array($key, $this->_fields)) {
                   $this->$key = $value;
               }
            }
            return true;
        } else {
            return false;
        }
        //переводит массив POST в переменные
    }

    public static function createInstance($db, $row)
    {
        $className = get_called_class();
        $instance = new $className($db);
        $instance->load($row);

        return $instance;
    }

    public function save()
    {
        if ($this->id) {
            $rows = '';
            foreach ($this->_fields as $field) {
                $rows .= '`'.$field.'` = "' . $this->$field . '",';
            }
            $rows = rtrim($rows, ',');

            $this->_db->query('UPDATE `' . static::$tableName . '` SET ' . $rows . ' WHERE `id` = ' . $this->id);
        } else {

            $names = implode(',', $this->_fields);
            $values = '';
            foreach ($this->_fields as $field) {
                $values .= '"' . $this->$field . '",';
            }
            $values = rtrim($values, ',');

            $this->_db->query('INSERT INTO `' . static::$tableName . '` ( ' . $names . ' ) VALUES (' . $values . ')');
            $this->id = $this->_db->lastInsertId();
        }
        return $this->id;
    }

    public function delete()
    {
        $this->_db->query('DELETE FROM `'. static::$tableName . '` WHERE id='.$this->id);
    }
}
