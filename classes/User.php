<?php
require_once 'Entry.php';

class User extends Entry
{
    protected $_fields = [
        'id',
        'login',
        'password',
    ];

    public static $tableName = "users";
}
