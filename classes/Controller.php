<?php

class Controller
{
   public $controller;
   public $action;

   public $db;

   public function render($path, $data = [])
   {
       require_once 'views/header.php';
       extract($data);
       require_once "views/{$this->controller}/$path.php";

       require_once 'views/footer.php';
       //передает во view параметры

   }
}
