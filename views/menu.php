<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">МойБлог!</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= $route == 'index' ? 'active' : '' ?>"><a href="index.php">Новости</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="<?= $route == 'auth' ? 'active' : '' ?>"><a href="index.php?action=login&controller=user">Войти</a>
                <li class="<?= $route == 'reg' ? 'active' : '' ?>"><a href="index.php?controller=user&action=register">Зарегистрироваться</a>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>