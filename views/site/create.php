<div class="container">

    <div class="form-container">
        <form class="form-horizontal" action="index.php?controller=site&action=create" method="post">
            <div class="form-group">
                <label for="articleHeader" class="col-sm-2 control-label">Название</label>
                <div class="col-sm-10">
                    <input name="title" type="text" class="form-control" id="articleHeader" placeholder="Название статьи">
                </div>
            </div>
            <div class="form-group">
                <label for="articleText" class="col-sm-2 control-label">Текст</label>
                <div class="col-sm-10">
                    <textarea name="text" id="articleText" class="form-control" rows="6" placeholder="Текст статьи"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-10">
                    <button type="submit" class="btn btn-success">Добавить</button>
                </div>
            </div>
        </form>
    </div>

</div><!-- /.container -->
