<div class="container">
    <h3><?=$post->title?></h3>
    <br>
    <?=$post->text?>
    <br>
    <?=$post->date?>
    <hr>


<?php
foreach ($comments as $comment) {
    $authorOfTheComment = $comment->getUser();
    include 'views/site/view_showComment.php';
}

if (isLoggedIn()) {
    include 'views/site/view_comment.php';
}
?>
</div>
