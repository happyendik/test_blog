<div class="form-container">
    <form class="form-horizontal" action="index.php?controller=comment&action=create&id=<?=$post->id?>" method="post">
        <div class="form-group">
            <label for="CommentText" class="col-sm-4 control-label">Комментарий</label>
            <div class="col-sm-10">
                <textarea name="text" id="CommentText" class="form-control" rows="6" placeholder="Текст комментария"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-10">
                <button name="submit" type="submit" class="btn btn-success">Добавить</button>
            </div>
        </div>
    </form>
</div>

</div>
