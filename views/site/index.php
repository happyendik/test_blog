<?php

foreach ($posts as $post) {
?>
    <div class="container">
        <h3><?= $post->title ?></h3>
        <p><?= $post->text ?></p>
        <a href="index.php?controller=site&action=view&id=<?=$post->id?>">комментарии <span class="badge"><?=count($post->getComments())?></span></a>
        <?php
        if (isLoggedIn()) {
            echo "<br><br><a class='btn btn-danger' href='index.php?controller=site&action=delete&id=$post->id' role='button'>Удалить</a>  ";
            echo "<a class='btn btn-warning' href='index.php?controller=site&action=edit&id=$post->id' role='button'>Редактировать</a>";
        }
        ?>
        <hr>
    </div>

<?php
}
