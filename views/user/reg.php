
<div class="container">

    <div class="form-container">
        <form class="form-horizontal" action="index.php?controller=user&action=register" method="post">
            <div class="form-group">
                <label for="inputLogin1" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input name="login" value="<?=$form->login?>" type="text" class="form-control" id="inputLogin1" placeholder="Логин">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword1" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input name= "password1" type="password" class="form-control" id="inputPassword1" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword2" class="col-sm-2 control-label">Пароль (Повтор)</label>
                <div class="col-sm-10">
                    <input name = "password2" type="password" class="form-control" id="inputPassword2" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name='submit' type="submit" class="btn btn-default">Зарегистрироваться</button>
                    <br><br>
                    Зарегистрированы? <a href="index.php?controller=user&action=login">Авторизируйтесь</a>
                </div>
            </div>
        </form>
    </div>

</div><!-- /.container -->
