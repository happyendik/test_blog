<div class="container">

    <div class="form-container">
        <form class="form-horizontal" action="index.php?controller=user&action=login" method="post">
            <div class="form-group">
                <label for="inputLogin1" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input name="login" value="<?=$form->login?>" type="text" class="form-control" id="inputLogin1" placeholder="Логин">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input name="password" value="" type="password" class="form-control" id="inputPassword" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button name='submit' type="submit" class="btn btn-default">Войти</button>
                    <br><br>
                    Нет аккаунта? <a href="index.php?controller=user&action=register">Зарегистрируйтесь</a>
                </div>
            </div>
        </form>
    </div>

</div><!-- /.container -->
