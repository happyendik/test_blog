<?php
require_once 'requireFirst.php';

if (isset($_GET['controller'])) {
    $controller = $_GET['controller'];
} else {
    $controller = 'site';
}
if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'index';
}

$className = ucfirst($controller).'Controller';
$actionName = 'action'.ucfirst($action);

$c = new $className;
$c->controller = $controller;
$c->action = $action;
$c->db = $db;
$c->$actionName();
