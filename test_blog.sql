-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Май 19 2017 г., 16:43
-- Версия сервера: 5.7.18-0ubuntu0.16.04.1
-- Версия PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `text`, `date`) VALUES
(1, 15, 6, '1212312323', '2017-05-15 12:16:46'),
(2, 15, 6, 'werwerwerwerwerwerwer', '2017-05-15 12:17:03'),
(3, 15, 6, 'Jopa', '2017-05-15 12:27:50'),
(4, 10, 5, 'london is the capital of great britain', '2017-05-15 12:29:55'),
(5, 11, 6, 'comment', '2017-05-15 12:48:27'),
(6, 15, 6, '000000000000000000000', '2017-05-15 12:57:10'),
(7, 15, 9, 'dddddddddddd', '2017-05-15 13:12:17'),
(8, 15, 9, 'ddd', '2017-05-15 13:12:29'),
(9, 11, 8, '77777777777777777', '2017-05-15 13:12:53'),
(10, 10, 9, 'Ð¡Ð¼Ñ‹ÑÐ»Ðµ', '2017-05-15 16:48:31'),
(11, 10, 9, 'ÑÑ‚Ð¾ ÐºÐ¾Ð¼Ð¼ÐµÐ½Ñ‚Ð°Ñ€Ð¸Ð¹ Ð’Ð°ÑÐ¸', '2017-05-15 16:49:03'),
(12, 10, 10, 'Ð­Ñ‚Ð¾ ÐºÐ¾Ð¼Ð¼ÐµÐ½Ñ‚Ð°Ñ€Ð¸Ð¹ ÐŸÐµÑ‚Ð¸', '2017-05-15 16:49:35'),
(13, 17, 9, 'Ð¡Ð°Ð¼ Ð½Ð°Ð¿Ð¸ÑÐ°Ð», ÑÐ°Ð¼ ÐºÐ¾Ð¼Ð¼ÐµÐ½Ñ‡Ñƒ', '2017-05-15 17:07:23'),
(14, 10, 9, 'qqweqwewe', '2017-05-15 17:34:01'),
(15, 16, 1, 'dfsdf', '2017-05-15 17:54:08'),
(16, 18, 1, 'Ð“Ð»ÑƒÐ¿Ð¾ ÐºÐ¾Ð¼Ð¼ÐµÐ½Ñ‚Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒ ÑÐ²Ð¾ÑŽ Ð¶Ðµ ÑÑ‚Ð°Ñ‚ÑŒÑŽ', '2017-05-16 11:03:15'),
(17, 18, 1, 'vasya', '2017-05-16 11:07:14'),
(18, 18, 1, 'vasya', '2017-05-16 11:08:19'),
(19, 18, 9, 'vasya', '2017-05-16 11:16:16'),
(20, 18, 9, 'vASYA', '2017-05-16 11:16:27'),
(21, 10, 11, 'fred here', '2017-05-17 16:59:39'),
(22, 111, 111, 'ewrwrerwrwerw', '2017-05-18 15:01:00'),
(23, 111, 111, 'ewrwrerwrwerw', '2017-05-18 15:01:51'),
(24, 111, 111, 'ewrwrerwrwerw', '2017-05-18 15:02:21'),
(25, 18, 9, '123', '2017-05-18 16:10:26'),
(26, 18, 9, '2222', '2017-05-18 16:10:30'),
(27, 16, 9, '11111111111111111111111111', '2017-05-18 16:10:53'),
(28, 21, 9, '123', '2017-05-18 16:12:20'),
(29, 22, 25, 'paul', '2017-05-18 16:13:42'),
(30, 16, 25, 'paul', '2017-05-18 16:13:49'),
(31, 1, 1, '', '2017-05-19 00:00:00'),
(32, 16, 9, '111111222222', '2017-05-19 14:56:49'),
(33, 11, 9, '3333', '2017-05-19 14:56:59'),
(34, 16, 28, 'eeeeee', '2017-05-19 16:39:36'),
(35, 16, 28, '123123qqaefd', '2017-05-19 16:39:40');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `text`, `date`) VALUES
(16, 9, 'ÐŸÐ¾Ð¼Ð¾Ñ‰ÑŒ Ð¿Ñ€Ð¾ÐµÐºÑ‚Ñƒ', 'ÐŸÐ¾ ÑÐ²Ð¾ÐµÐ¹ ÑÑƒÑ‚Ð¸ Ñ€Ñ‹Ð±Ð°Ñ‚ÐµÐºÑÑ‚ ÑÐ²Ð»ÑÐµÑ‚ÑÑ Ð°Ð»ÑŒÑ‚ÐµÑ€Ð½Ð°Ñ‚Ð¸Ð²Ð¾Ð¹ Ñ‚Ñ€Ð°Ð´Ð¸Ñ†Ð¸Ð¾Ð½Ð½Ð¾Ð¼Ñƒ lorem ipsum, ÐºÐ¾Ñ‚Ð¾Ñ€Ñ‹Ð¹ Ð²Ñ‹Ð·Ñ‹Ð²Ð°ÐµÑ‚ Ñƒ Ð½ÐµÐºÑ‚Ð¾Ñ€Ñ‹Ñ… Ð»ÑŽÐ´ÐµÐ¹ Ð½ÐµÐ´Ð¾ÑƒÐ¼ÐµÐ½Ð¸Ðµ Ð¿Ñ€Ð¸ Ð¿Ð¾Ð¿Ñ‹Ñ‚ÐºÐ°Ñ… Ð¿Ñ€Ð¾Ñ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ Ñ€Ñ‹Ð±Ñƒ Ñ‚ÐµÐºÑÑ‚. Ð’ Ð¾Ñ‚Ð»Ð¸Ñ‡Ð¸Ð¸ Ð¾Ñ‚ lorem ipsum, Ñ‚ÐµÐºÑÑ‚ Ñ€Ñ‹Ð±Ð° Ð½Ð° Ñ€ÑƒÑÑÐºÐ¾Ð¼ ÑÐ·Ñ‹ÐºÐµ Ð½Ð°Ð¿Ð¾Ð»Ð½Ð¸Ñ‚ Ð»ÑŽÐ±Ð¾Ð¹ Ð¼Ð°ÐºÐµÑ‚ Ð½ÐµÐ¿Ð¾Ð½ÑÑ‚Ð½Ñ‹Ð¼ ÑÐ¼Ñ‹ÑÐ»Ð¾Ð¼ Ð¸ Ð¿Ñ€Ð¸Ð´Ð°ÑÑ‚ Ð½ÐµÐ¿Ð¾Ð²Ñ‚Ð¾Ñ€Ð¸Ð¼Ñ‹Ð¹ ÐºÐ¾Ð»Ð¾Ñ€Ð¸Ñ‚ ÑÐ¾Ð²ÐµÑ‚ÑÐºÐ¸Ñ… Ð²Ñ€ÐµÐ¼ÐµÐ½.', '2017-05-15 16:47:33'),
(10, 11, 'Ð¡Ð¼Ñ‹ÑÐ» ÑÐ°Ð¹Ñ‚Ð°', 'FRED EDIT Ð½ÐµÐ¿Ð¾Ð½ÑÑ‚Ð½Ñ‹Ð¼ ÑÐ¼Ñ‹ÑÐ»Ð¾Ð¼ Ð¸ Ð¿Ñ€Ð¸Ð´Ð°ÑÑ‚ Ð½ÐµÐ¿Ð¾Ð²Ñ‚Ð¾Ñ€Ð¸Ð¼Ñ‹Ð¹ ÐºÐ¾Ð»Ð¾Ñ€Ð¸Ñ‚ ÑÐ¾Ð²ÐµÑ‚ÑÐºÐ¸Ñ… Ð²Ñ€ÐµÐ¼ÐµÐ½', '2017-05-17 16:59:01'),
(22, 25, 'PAUL18OfMay', 'PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay PAUL18OfMay ', '2017-05-18 16:13:27'),
(11, 6, 'Ð•Ñ‰Ðµ Ð¾Ð´Ð½Ð° ÑÑ‚Ð°Ñ‚ÑŒÑ', 'ÐŸÐ¾Ñ‡ÐµÐ¼Ñƒ-Ñ‚Ð¾ ÑÐ°Ð¹Ñ‚ Ñ€Ð°Ð±Ð¾Ñ‚Ð°ÐµÑ‚, Ñ…Ð¾Ñ‚Ñ Ð½Ð° Ð²Ð¸Ð½Ð´Ðµ Ð²Ñ‹Ð´Ð°ÐµÑ‚ Ð¾ÑˆÐ¸Ð±ÐºÑƒ. ', '2017-05-15 09:55:15');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'test', 'cdf26213a150dc3ecb610f18f6b38b46'),
(2, '123', 'cdf26213a150dc3ecb610f18f6b38b46'),
(3, 'happyendik', '781f357c35df1fef3138f6d29670365a'),
(4, '333', '638931d8701fbe6a2625549b934ea9b4'),
(5, '444', '7ac16912d528198c5f272be9c8d0a539'),
(6, '000', '3fff4de387daeecc0897fb6f16d25ac8'),
(7, '666', '82a3ce9a08d9555f99cfcb74c596bd3a'),
(8, '777', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(9, 'vasya', '781f357c35df1fef3138f6d29670365a'),
(10, 'petya', '781f357c35df1fef3138f6d29670365a'),
(11, 'fred', '781f357c35df1fef3138f6d29670365a'),
(12, 'Vasiliy', '781f357c35df1fef3138f6d29670365a'),
(13, 'Tommy', '781f357c35df1fef3138f6d29670365a'),
(15, 'John', '781f357c35df1fef3138f6d29670365a'),
(16, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(17, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(18, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(19, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(20, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(21, 'John', '781f357c35df1fef3138f6d29670365a'),
(22, 'Mike', 'ae5e5da6b3ea541a08f8139bf8743ed6'),
(23, 'John', '781f357c35df1fef3138f6d29670365a'),
(24, 'John', '781f357c35df1fef3138f6d29670365a'),
(25, 'paul', '781f357c35df1fef3138f6d29670365a'),
(26, 'popa', ''),
(27, 'lalalal', ''),
(28, 'eeedddd', '781f357c35df1fef3138f6d29670365a');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
