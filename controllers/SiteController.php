<?php

class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->render('index', ['posts' => Post::findAll($this->db)]);
    }

    public function actionCreate()
    {
        if (!isLoggedIn()) {
            header('Location: index.php?controller=user&action=login');
            exit();
        }

        $post = new Post($this->db);
        if ($post->load($_POST)) {
            if ($post->title == '' || $post->text == '') {
                echo 'Поля заполнены неверно';
            } else {
                $post->user_id = $_SESSION['id'];
                $post->date = date('Y-m-d H:i:s');
                $post->save();
                header('Location: index.php');
                exit();
            }
        }

        $this->render('create');
    }

    public function actionEdit()
    {
        if (!isLoggedIn()) {
            header('Location: index.php?controller=user&action=login');
            exit();
        }

        if (isset($_GET['id'])) {
            $post = Post::findOne($this->db, $_GET['id']);
            if ($post->load($_POST)) {
                $post->user_id = $_SESSION['id'];
                $post->date = date('Y-m-d H:i:s');
                if ($post->save()) {
                    header('Location: index.php?controller=site&action=view&id=' . $_GET['id']);
                    exit();
                }

            } else {
                $this->render('edit', [
                    'post' => $post,
                ]);
            }
        }
    }

    public function actionView()
    {
        if (isset($_GET['id'])) {
            $post = Post::findOne($this->db, $_GET['id']);
            $comments = $post->getComments();

            $this->render('view', [
                'post' => $post,
                'comments' => $comments,
            ]);
        }
    }

    public function actionDelete()
    {
        if (isset($_GET['id'])) {
            $post = Post::findOne($this->db, $_GET['id']);
            $post->delete();
            header('Location: index.php');
        }
    }
}
