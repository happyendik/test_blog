<?php

 class UserController extends Controller
 {
     public function actionLogin()
     {
         $form = new User($this->db);
         if ($form->load($_POST)) {
             $user = User::findOneByAttribute($this->db, 'login', $form->login);
             $hash = hash('ripemd128', $form->password);
             if (!$user || $hash != $user->password) {
                 echo 'Неверные данные';
             } else {
                 $_SESSION['id'] = $user->id;
                 header('Location: index.php?controller=site&action=index');
                 exit();
             }
         }
         $this->render('auth', [
             'form' => $form,
         ]);
     }

     public function actionLogout()
     {
         $_SESSION = array();
         if (session_id() != "" || isset($_COOKIE[session_name()]))
             setcookie(session_name(), '', time()-2592000, '/');
         session_destroy();
         header('Location: index.php');
     }

     public function actionRegister()
     {
         $form = new User($this->db);
         if ($form->load($_POST)) {
             $user = User::findOneByAttribute($this->db, 'login', $form->login);
             if ($user || $form->password1 != $form->password2) {
                echo 'Данные введены неверно или пользователь уже существует';
             } else {
                 $user = new User($this->db);
                 $user->password = hash('ripemd128', $form->password1);
                 $user->login = $form->login;
                 $_SESSION['id'] = $user->save();
                 header('Location: index.php');
                 exit();
             }
         }
         $this->render('reg', [
             'form' => $form,
         ]);
     }
 }
