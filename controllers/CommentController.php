<?php

class CommentController extends Controller
{
    public function actionCreate()
    {
        if (isset($_GET['id'])) {
            $comment = new Comment($this->db);
        }
        if ($comment->load($_POST)) {
            $comment->user_id = $_SESSION['id'];
            $comment->post_id = $_GET['id'];
            $comment->date = date("Y-m-d H:i:s");
            $comment->save();
            header("Location: index.php?controller=site&action=view&id={$_GET['id']}");
            exit();
        }
    }
}
