<?php
error_reporting(E_ALL);
session_start();

require_once 'db_config.php';
require_once 'classes/DataBase.php';
require_once 'classes/User.php';
require_once 'classes/Post.php';
require_once 'classes/Comment.php';
require_once 'classes/Controller.php';
require_once 'controllers/SiteController.php';
require_once 'controllers/UserController.php';
require_once 'controllers/CommentController.php';


$db = new DataBase($db_host, $db_user, $db_pass, $db_name);

function isLoggedIn()
{
    return isset($_SESSION['id']);
}

list($route) = explode('.php', $_SERVER['REQUEST_URI']);
$route = ltrim($route, '/');

